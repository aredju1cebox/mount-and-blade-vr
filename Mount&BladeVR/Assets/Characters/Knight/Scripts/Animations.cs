﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animations : MonoBehaviour {

    public Animator anim;
    private CharacterController controller;
    private bool onGround;
   

	// Use this for initialization
	void Start ()
    {
        //Gets animator on character
        anim = GetComponent<Animator>();
        //Get controller on character
        controller = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Setting Idle animations as default
        anim.SetFloat("Speed", 0);
        anim.SetFloat("Strafe", 0);

        //-----Movement related animations------
        //On W press set speed to 1
        if (Input.GetKey(KeyCode.W))//W
        {
            anim.SetFloat("Speed", 1);
            
        }
        //On S press set speed -1
        if (Input.GetKey(KeyCode.S))//S
        {
            anim.SetFloat("Speed", -1);
            
        }
        //Strafe right
        if (Input.GetKey(KeyCode.D))//D
        {
            anim.SetFloat("Strafe", 1);
            
        }
        //Strafe left
        if (Input.GetKey(KeyCode.A))//A
        {
            anim.SetFloat("Strafe", -1);
            
        }




        //to check if character is grounded
        if (controller.isGrounded)
        {
            onGround = true;
        }
        else
        {
            onGround = false;
        }
    }
}

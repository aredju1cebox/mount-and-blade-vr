﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*****************************************************************************
 * Class: Movement (extends MonoBehaviour)
 * 
 * This class is designed to hold methods related to movement controls
 * for a GameObject with a CharacterController attached. This includes:
 * 		
 * 		1. Walking in all eight directions.
 * 		2. Sprinting (NYI).
 * 		3. Jumping.
 * 		4. Key-based character rotation.
 * 
 * This does not include functionality for mouselook. For functionality
 * related to mouse-based character control, please refer to the <ClassName>
 * class located in <UnityPathToClassFile>.
 * 
 *****************************************************************************/

public class Movement : MonoBehaviour {

    // Create a vector for storing movement calculations
    private Vector3 movementVector;

	// Create a CharacterController variable for this GameObject
    private CharacterController controller;

	// A floating point value for movement speed (default = 5.0f)
    public float MovementSpeed = 5.0f;
    
	// A floating point value for jump height (default = 15.0f)
	public float JumpPower = 15.0f;

	// A floating point value for gravity intensity (default = 40.0f)
    public float gravity = 40.0f;

	// A floating point value for rotation speed (default = 5.0f)
    public float RotateSpeed = 5.0f;

	// A floating point value for rotation or something. I don't fucking know. (default = 0.0f)
    private float rotate = 0.0f;

	// Post-Awake Initialization Method for Key Game-Related Systems
	void Start ()
    {
        // Retrieve the first CharacterController attached to this GameObject
        controller = GetComponent<CharacterController>();
	}
	
	// Standard per-frame update method
	void Update ()
    {
		// If the W key is pressed, move this GameObject Forward
		if (Input.GetKey(KeyCode.W)) 
        	transform.position += transform.forward * Time.deltaTime * MovementSpeed;

		// If the S key is pressed, move this GameObject Backward
		if (Input.GetKey(KeyCode.S)) 
            transform.position -= transform.forward * Time.deltaTime * (MovementSpeed-2);

		// If the A key is pressed, strafe this GameObject to the Left
		if (Input.GetKey(KeyCode.A)) 
            transform.position -= transform.right * Time.deltaTime * MovementSpeed;

		// If the D key is pressed, strafe this GameObject to the Right
		if (Input.GetKey(KeyCode.D)) 
            transform.position += transform.right * Time.deltaTime * MovementSpeed;

		// If the Q key is pressed, rotate this GameObject to the Left
		if (Input.GetKey(KeyCode.Q)) 
            transform.Rotate(0, 3, 0);

		// If the E key is pressed, stafe this GameObject to the Right
		if (Input.GetKey(KeyCode.E)) 
            transform.Rotate(0, -3, 0);

		// If the CharacterController is touching the ground, initialize the movement vector's
		// Y value to 0. Next, check if the Space key has been pressed. If so, set the Y value
		// to JumpPower and continue.
		if (controller.isGrounded)
        {
            movementVector.y = 0;

            if (Input.GetKeyDown(KeyCode.Space))
         	   movementVector.y = JumpPower;
        }

		// I get what you're trying to do, but I feel like it doesn't complete the jumping effect
		movementVector.y -= gravity * Time.deltaTime;

		// Move the CharacterController based on the jumping calculations
        controller.Move(movementVector * Time.deltaTime);
    }
}

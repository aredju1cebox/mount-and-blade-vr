﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateGirl : MonoBehaviour {

	public Animation anim;

	void Start() 
	{
		anim = GetComponent<Animation>();
		anim.enabled = true;
	}


	void Update () {
		if (!anim.isPlaying)
			anim.Play ("Idle");
	}
}
